<?php

namespace App\Tests\Unit\Shared\Infrastructure\Database;

use App\Shared\Infrastructure\Database\JsonDBEngine;
use PHPUnit\Framework\TestCase;

class JsonDBEngineTest extends TestCase
{
    public function test_it_throw_exception_when_file_doesnt_exist(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectErrorMessage("Json file doesn't exist");

        $engine = new JsonDBEngine("data/invalid.json");
        $engine->all();
    }

    public function test_it_throw_exception_when_file_is_invalid(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectErrorMessage("Json file is invalid");

        $engine = new JsonDBEngine(__DIR__ . "/invalid_db.json");
        $engine->all();
    }

    public function test_it_return_db(): void
    {
        $engine = new JsonDBEngine(__DIR__ . "/good_db.json");
        $data = $engine->all();

        $this->assertIsArray($data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('cars', $data);
        $this->assertArrayHasKey('address', $data);
        $this->assertIsArray($data['cars']);
        $this->assertIsArray($data['address']);
        $this->assertEquals("testing name", $data['name']);
        $this->assertCount(2, $data['cars']);

        $this->assertEquals("bmw", $data['cars'][1]);
        $this->assertIsArray($data['address']);
        $this->assertEquals("Torun", $data['address']['city']);
    }
}

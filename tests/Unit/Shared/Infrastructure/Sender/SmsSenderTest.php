<?php

namespace App\Tests\Unit\Shared\Infrastructure\Sender;

use App\Shared\Domain\Writer\Writer;
use App\Shared\Infrastructure\Sender\SmsSender;
use PHPUnit\Framework\TestCase;

class SmsSenderTest extends TestCase
{
    public function test_it_send_sms(): void
    {
        $target = 'jan@nowak.pl';
        $message = "test";

        $writer = $this->getMockBuilder(Writer::class)->getMock();
        $writer
            ->expects(self::once())
            ->method('write')
            ->with(sprintf('%s: %s'.PHP_EOL, $target, $message));

        $sender = new SmsSender($writer);
        $sender->send($target, $message);
    }
}

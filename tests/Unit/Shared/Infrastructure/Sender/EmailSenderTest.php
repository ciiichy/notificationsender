<?php

namespace App\Tests\Unit\Shared\Infrastructure\Sender;

use App\Shared\Infrastructure\Sender\EmailSender;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailSenderTest extends TestCase
{
    public function test_it_send_email(): void
    {
        $target = 'jan@nowak.pl';
        $message = "test";

        $mailer = $this->getMockBuilder(MailerInterface::class)->getMock();
        $mailer
            ->expects(self::once())
            ->method('send')
            ->with($this->callback(
                function (Email $email) {
                    $this->assertInstanceOf(Email::class, $email);
                    $this->assertEquals('test', $email->getTextBody());
                    $this->assertEquals('send-notification@test.pl', $email->getFrom()[0]->toString());
                    $this->assertEquals('jan@nowak.pl', $email->getTo()[0]->toString());

                    return true;
                })
            );

        $sender = new EmailSender($mailer);
        $sender->send($target, $message);
    }
}

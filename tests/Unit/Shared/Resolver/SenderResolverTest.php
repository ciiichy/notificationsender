<?php

namespace App\Tests\Unit\Shared\Resolver;

use App\Shared\Domain\Writer\Writer;
use App\Shared\Infrastructure\Resolver\SenderResolver;
use App\Shared\Infrastructure\Sender\EmailSender;
use App\Shared\Infrastructure\Sender\SmsSender;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;

class SenderResolverTest extends TestCase
{
    public function test_it_resolve_sms_and_email(): void
    {
        $writer = $this->getMockBuilder(Writer::class)->getMock();
        $smsSender = new SmsSender($writer);

        $mailer = $this->getMockBuilder(MailerInterface::class)->getMock();
        $emailSender = new EmailSender($mailer);

        $resolver = new SenderResolver([$smsSender, $emailSender]);
        $this->assertEquals([$smsSender,$emailSender], $resolver->resolve(['sms', 'email']));
    }

    public function test_it_resolve_sms(): void
    {
        $writer = $this->getMockBuilder(Writer::class)->getMock();
        $smsSender = new SmsSender($writer);

        $mailer = $this->getMockBuilder(MailerInterface::class)->getMock();
        $emailSender = new EmailSender($mailer);

        $resolver = new SenderResolver([$smsSender, $emailSender]);
        $this->assertEquals([$smsSender], $resolver->resolve(['sms']));
    }

    public function test_it_resolve_email(): void
    {
        $writer = $this->getMockBuilder(Writer::class)->getMock();
        $smsSender = new SmsSender($writer);

        $mailer = $this->getMockBuilder(MailerInterface::class)->getMock();
        $emailSender = new EmailSender($mailer);


        $resolver = new SenderResolver([$smsSender, $emailSender]);
        $this->assertEquals([$emailSender], $resolver->resolve(['email']));
    }

    public function test_it_resolve_nothing(): void
    {
        $writer = $this->getMockBuilder(Writer::class)->getMock();
        $smsSender = new SmsSender($writer);

        $mailer = $this->getMockBuilder(MailerInterface::class)->getMock();
        $emailSender = new EmailSender($mailer);

        $resolver = new SenderResolver([$smsSender, $emailSender]);
        $this->assertEquals([], $resolver->resolve([]));
    }
}

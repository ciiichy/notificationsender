<?php

namespace App\Tests\Unit\Core\Application\CommandHandler;

use App\Core\Application\Command\SendNotificationCommand;
use App\Core\Application\CommandHandler\SendNotificationHandler;
use App\Core\Domain\Policy\ChannelsPolicy;
use App\Shared\Domain\Sender\Sender;
use App\Shared\Infrastructure\Resolver\Resolver;
use PHPUnit\Framework\TestCase;

class SendNotificationHandlerTest extends TestCase
{
    public function test_it_send_sms_and_email(): void
    {
        $channels = ['sms', 'email'];
        $command = new SendNotificationCommand('adam@kowalski', 'text', $channels);

        $smsSender = $this->getMockBuilder(Sender::class)->getMock();
        $smsSender
            ->expects(self::once())
            ->method('send')
            ->with('adam@kowalski', 'text');

        $emailSender = $this->getMockBuilder(Sender::class)->getMock();
        $emailSender
            ->expects(self::once())
            ->method('send')
            ->with('adam@kowalski', 'text');

        $channelsPolicy = $this->getMockBuilder(ChannelsPolicy::class)->getMock();
        $channelsPolicy
            ->expects(self::once())
            ->method('execute')
            ->with($channels)
            ->willReturn($channels);

        $senderResolver = $this->getMockBuilder(Resolver::class)->getMock();
        $senderResolver
            ->expects(self::once())
            ->method('resolve')
            ->with($channels)
            ->willReturn([$smsSender, $emailSender]);

        $handler = new SendNotificationHandler($senderResolver, $channelsPolicy);
        $handler->__invoke($command);
    }
}

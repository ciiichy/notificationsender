<?php

namespace App\Tests\Unit\Core\Application\View;

use App\Core\Application\View\UserView;
use PHPUnit\Framework\TestCase;

class UserViewTest extends TestCase
{
    public function test_it_create_user_view(): void
    {
        $userView = new UserView("adam.nowak", "adam@nowak.pl", ["sms", "email"]);

        $this->assertEquals('adam.nowak', $userView->login());
        $this->assertEquals('adam@nowak.pl', $userView->email());
        $this->assertEquals(["sms", "email"], $userView->contactChannels());
    }

    public function test_it_return_array(): void
    {
        $expected = [
            'login' => 'adam.nowak',
            'email' => 'adam@nowak.pl',
            'contactChannels' => ['sms', 'email']
        ];

        $userView = new UserView("adam.nowak", "adam@nowak.pl", ["sms", "email"]);

        $this->assertEquals($expected, $userView->toArray());
    }
}

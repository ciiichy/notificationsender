<?php

namespace App\Tests\Unit\Core\Application\Query;

use App\Core\Application\Query\GetUserByLoginQuery;
use PHPUnit\Framework\TestCase;

class GetUserByLoginQueryTest extends TestCase
{
    public function test_it_create_query(): void
    {
        $query = new GetUserByLoginQuery('test_login');

        $this->assertEquals('test_login', $query->login());
    }
}

<?php

namespace App\Tests\Unit\Core\Application\QueryHandler;

use App\Core\Application\Query\GetUsersQuery;
use App\Core\Application\QueryHandler\GetUsersHandler;
use App\Core\Application\Repository\Users;
use App\Core\Application\View\UserView;
use PHPUnit\Framework\TestCase;

class GetUsersHandlerTest extends TestCase
{
    public function test_it_get_users(): void
    {
        $query = new GetUsersQuery();
        $expected = [
            new UserView('adam', 'adam@nowak.pl', []),
            new UserView('jan', 'jan@nowak.pl', ['sms','email'])
        ];

        $users = $this->getMockBuilder(Users::class)->getMock();
        $users
            ->expects(self::once())
            ->method('all')
            ->willReturn($expected);

        $handler = new GetUsersHandler($users);

        $data = $handler->__invoke($query);

        $this->assertEquals($expected, $data);
    }
}

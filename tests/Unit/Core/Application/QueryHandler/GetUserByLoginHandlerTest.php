<?php

namespace App\Tests\Unit\Core\Application\QueryHandler;

use App\Core\Application\Query\GetUserByLoginQuery;
use App\Core\Application\QueryHandler\GetUserByLoginHandler;
use App\Core\Application\Repository\Users;
use App\Core\Application\View\UserView;
use PHPUnit\Framework\TestCase;

class GetUserByLoginHandlerTest extends TestCase
{
    public function test_it_get_user_by_login(): void
    {
        $query = new GetUserByLoginQuery('adam');
        $expected = new UserView('adam', 'adam@nowak.pl', []);

        $users = $this->getMockBuilder(Users::class)->getMock();
        $users
            ->expects(self::once())
            ->method('byLogin')
            ->with('adam')
            ->willReturn($expected);

        $handler = new GetUserByLoginHandler($users);

        $data = $handler->__invoke($query);

        $this->assertEquals($expected, $data);
    }
}

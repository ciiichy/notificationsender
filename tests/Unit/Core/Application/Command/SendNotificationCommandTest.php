<?php

namespace App\Tests\Unit\Core\Application\Command;

use App\Core\Application\Command\SendNotificationCommand;
use PHPUnit\Framework\TestCase;

class SendNotificationCommandTest extends TestCase
{
    public function test_it_create_command(): void
    {
        $command = new SendNotificationCommand("a@wp.pl", "text", ["sms", "email"]);

        $this->assertEquals("a@wp.pl", $command->target());
        $this->assertEquals("text", $command->message());
        $this->assertEquals(["sms", "email"], $command->channels());
    }
}

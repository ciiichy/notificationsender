<?php

namespace App\Tests\Unit\Core\UI\HTTP\API;

use App\Core\Application\Query\GetUsersQuery;
use App\Core\Application\View\UserView;
use App\Core\UI\HTTP\API\GetAllUsersAction;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class GetAllUsersActionTest extends TestCase
{
    public function test_get_get_all_users(): void
    {
        $data = [
            new UserView("adam", "adam@nowak.pl", ['sms']),
            new UserView("jan", "jan@nowak.pl", ['sms','email']),
        ];

        $stamp = new HandledStamp($data, "stamp");
        $query = new GetUsersQuery();
        $envelope = new Envelope($query, [$stamp]);

        $queryBus = $this->getMockBuilder(MessageBusInterface::class)->getMock();
        $queryBus
            ->expects(self::once())
            ->method('dispatch')
            ->with($this->isInstanceOf(GetUsersQuery::class))
            ->willReturn($envelope);

        $controller = new GetAllUsersAction($queryBus);

        $result = $controller->__invoke();

        $this->assertInstanceOf(JsonResponse::class, $result);
        $resultData = json_decode($result->getContent(), true);

        $expected = [
            [
                'login' => 'adam',
                'email' => 'adam@nowak.pl',
                'contactChannels' => ['sms']
            ],
            [
                'login' => 'jan',
                'email' => 'jan@nowak.pl',
                'contactChannels' => ['sms', 'email']
            ],
        ];

        $this->assertEquals($expected, $resultData);
    }
}

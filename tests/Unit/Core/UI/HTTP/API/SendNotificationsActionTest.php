<?php

namespace App\Tests\Unit\Core\UI\HTTP\API;

use App\Core\Application\Command\SendNotificationCommand;
use App\Core\Application\Query\GetUserByLoginQuery;
use App\Core\Application\Query\GetUsersQuery;
use App\Core\Application\View\UserView;
use App\Core\UI\HTTP\API\SendNotificationsAction;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class SendNotificationsActionTest extends TestCase
{
    public function test_it_send_notification_to_two_users(): void
    {
        $json = '{
            "logins" : ["adam", "jan"],
            "message" : "test"
        }';

        $request = new Request([],[],[],[],[],[], $json);

        $stamp1 = new HandledStamp(new UserView('adam', 'adam@nowak.pl', ['sms']), "h");
        $query1 = new GetUsersQuery();
        $envelope1 = new Envelope($query1, [$stamp1]);

        $stamp2 = new HandledStamp(new UserView('jan', 'jan@nowak.pl', ['sms', 'email']), "h");
        $query2 = new GetUsersQuery();
        $envelope2 = new Envelope($query2, [$stamp2]);

        $queryBus = $this->getMockBuilder(MessageBusInterface::class)->getMock();
        $queryBus
            ->expects(self::exactly(2))
            ->method('dispatch')
            ->withConsecutive(
                [$this->isInstanceOf(GetUserByLoginQuery::class)],
                [$this->isInstanceOf(GetUserByLoginQuery::class)],
            )
            ->willReturnOnConsecutiveCalls($envelope1, $envelope2);

        $commandBus = $this->getMockBuilder(MessageBusInterface::class)->getMock();
        $commandBus
            ->expects(self::exactly(2))
            ->method('dispatch')
            ->withConsecutive(
                [
                    $this->callback(
                        function ($command) {
                            $this->assertInstanceOf(SendNotificationCommand::class, $command);
                            $this->assertEquals('test', $command->message());
                            $this->assertEquals(['sms'], $command->channels());
                            $this->assertEquals('adam@nowak.pl', $command->target());

                            return true;
                        }
                    )
                ],
                [$this->callback(
                    function ($command) {
                        $this->assertInstanceOf(SendNotificationCommand::class, $command);
                        $this->assertEquals('test', $command->message());
                        $this->assertEquals(['sms', 'email'], $command->channels());
                        $this->assertEquals('jan@nowak.pl', $command->target());

                        return true;
                    }
                )],
            )->willReturn(
                new Envelope(new \stdClass()),
                new Envelope(new \stdClass())
            );

        $controller = new SendNotificationsAction($commandBus, $queryBus);

        $result = $controller->__invoke($request);

        $this->assertInstanceOf(JsonResponse::class, $result);
    }

    public function test_it_throw_exception_for_invalid_empty_logins(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectErrorMessage("Invalid logins");

        $json = '{
            "logins" : [],
            "message" : "test"
        }';

        $request = new Request([],[],[],[],[],[], $json);

        $queryBus = $this->getMockBuilder(MessageBusInterface::class)->getMock();
        $queryBus
            ->expects(self::never())
            ->method('dispatch');

        $commandBus = $this->getMockBuilder(MessageBusInterface::class)->getMock();
        $commandBus
            ->expects(self::never())
            ->method('dispatch');

        $controller = new SendNotificationsAction($commandBus, $queryBus);

        $result = $controller->__invoke($request);

        $this->assertInstanceOf(JsonResponse::class, $result);
    }

    public function test_it_throw_exception_for_invalid_request(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectErrorMessage("Invalid request");

        $json = '{x
            "logins" : [],
            "message" : "test"
        }';

        $request = new Request([],[],[],[],[],[], $json);

        $queryBus = $this->getMockBuilder(MessageBusInterface::class)->getMock();
        $queryBus
            ->expects(self::never())
            ->method('dispatch');

        $commandBus = $this->getMockBuilder(MessageBusInterface::class)->getMock();
        $commandBus
            ->expects(self::never())
            ->method('dispatch');

        $controller = new SendNotificationsAction($commandBus, $queryBus);

        $result = $controller->__invoke($request);

        $this->assertInstanceOf(JsonResponse::class, $result);
    }
}

<?php

namespace App\Tests\Unit\Core\Domain\Policy;

use App\Core\Domain\Policy\DefaultEmailChannelsPolicy;
use PHPUnit\Framework\TestCase;

class DefaultEmailChannelsPolicyTest extends TestCase
{
    /**
     * @dataProvider providerData
     */
    public function test_it_return_correct_channels(array $expectedChannels, array $channels): void
    {
        $policy = new DefaultEmailChannelsPolicy();

        $this->assertEquals($expectedChannels, $policy->execute($channels));
    }

    public function providerData(): array
    {
        return [
            [['sms', 'email'], ['sms', 'email']],
            [['sms'], ['sms']],
            [['email'], ['email']],
            [['email'], []]
        ];
    }
}

<?php

namespace App\Tests\Unit\Core\Infrastructure\Repository\Json;

use App\Core\Application\View\UserView;
use App\Core\Infrastructure\Repository\Json\JsonUsers;
use App\Shared\Infrastructure\Database\DBEngine;
use PHPUnit\Framework\TestCase;

class JsonRepositoryTest extends TestCase
{
    private const DB_TEST_DATA = [
        [
            'login' => 'adam.kowalski',
            'email' => 'adam@kowalski.pl',
            'contact_channels' => "sms,email"
        ],
        [
            'login' => 'piotr.cichocki',
            'email' => 'piotr@cichocki.pl',
            'contact_channels' => "sms"
        ],
        [
            'login' => 'jan.nowak',
            'email' => 'jan@nowak.pl',
            'contact_channels' => null
        ],
    ];

    public function test_it_fetch_all_users(): void
    {
        $dbEngineMock = $this->getMockBuilder(DBEngine::class)->getMock();
        $dbEngineMock
            ->expects(self::once())
            ->method('all')
            ->willReturn(self::DB_TEST_DATA);

        $repository = new JsonUsers($dbEngineMock);
        $data = $repository->all();

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertInstanceOf(UserView::class, $data[0]);
        $this->assertInstanceOf(UserView::class, $data[1]);
        $this->assertInstanceOf(UserView::class, $data[2]);

        $this->assertEquals('adam.kowalski', $data['0']->login());
        $this->assertEquals('piotr.cichocki', $data['1']->login());
        $this->assertEquals('jan.nowak', $data['2']->login());

        $this->assertEquals('adam@kowalski.pl', $data['0']->email());
        $this->assertEquals('piotr@cichocki.pl', $data['1']->email());
        $this->assertEquals('jan@nowak.pl', $data['2']->email());

        $this->assertEquals(['sms', 'email'], $data['0']->contactChannels());
        $this->assertEquals(['sms'], $data['1']->contactChannels());
        $this->assertEquals([], $data['2']->contactChannels());
    }

    public function test_it_get_existed_user(): void
    {
        $dbEngineMock = $this->getMockBuilder(DBEngine::class)->getMock();
        $dbEngineMock
            ->expects(self::once())
            ->method('all')
            ->willReturn(self::DB_TEST_DATA);

        $repository = new JsonUsers($dbEngineMock);
        $user = $repository->byLogin('piotr.cichocki');

        $this->assertInstanceOf(UserView::class, $user);
        $this->assertEquals('piotr.cichocki', $user->login());
        $this->assertEquals('piotr@cichocki.pl', $user->email());
        $this->assertEquals(['sms'], $user->contactChannels());
    }

    public function test_it_return_null_for_not_existed_user(): void
    {
        $dbEngineMock = $this->getMockBuilder(DBEngine::class)->getMock();
        $dbEngineMock
            ->expects(self::once())
            ->method('all')
            ->willReturn(self::DB_TEST_DATA);

        $repository = new JsonUsers($dbEngineMock);
        $user = $repository->byLogin('kasia.kowalska');

        $this->isNull($user);
    }
}

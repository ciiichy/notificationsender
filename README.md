# INSTALLATION

```bash
docker-compose up -d
docker-compose exec php-fpm bash
composer install
```

# SEND NOTIFICATION VIA CLI
```bash
bin/console a:s -u username -m "message"
```

# SEND NOTIFICATION VIA API
```
[POST] localhost:8000/api/send-notification
BODY EXAMPLE:
{
    "logins" : ["jan.kowalski", "andrzej.kowalski", "marek.kowalski"],
    "message" : "info test 2"
}

```

# UNIT TEST
```
vendor/phpunit/phpunit/phpunit 
```

# TEST EMAILS INBOX
```
mailtrap.io
login: piotr@silentus.pl
password: Z3Y-7xuxSq$X_#x
https://mailtrap.io/inboxes/1026377/messages
```

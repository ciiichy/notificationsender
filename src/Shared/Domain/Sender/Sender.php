<?php

namespace App\Shared\Domain\Sender;

interface Sender
{
    public function send(string $target, string $message): void;

    public function acceptChannel(): string;
}

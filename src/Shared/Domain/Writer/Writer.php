<?php

namespace App\Shared\Domain\Writer;

interface Writer
{
    public function write(string $text): void;
}

<?php

namespace App\Shared\UI;

use App\Shared\Infrastructure\MessageBus\Query;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

trait QueryBusTrait
{
    protected MessageBusInterface $queryBus;

    protected function ask(Query $query)
    {
        $envelope = $this->queryBus->dispatch($query);

        /** @var HandledStamp $stamp */
        $stamp = $envelope->last(HandledStamp::class);

        return $stamp->getResult();
    }
}

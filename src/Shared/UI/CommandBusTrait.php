<?php

namespace App\Shared\UI;

use App\Shared\Infrastructure\MessageBus\Command;
use Symfony\Component\Messenger\MessageBusInterface;

trait CommandBusTrait
{
    protected MessageBusInterface $commandBus;

    protected function exec(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}

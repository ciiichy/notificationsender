<?php

namespace App\Shared\Infrastructure\Database;

interface DBEngine
{
    public function all(): array;
}

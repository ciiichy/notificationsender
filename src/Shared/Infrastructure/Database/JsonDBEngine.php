<?php

namespace App\Shared\Infrastructure\Database;

final class JsonDBEngine implements DBEngine
{
    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function all(): array
    {
        if (file_exists($this->filePath) === false) {
            throw new \RuntimeException("Json file doesn't exist");
        }

        $data = json_decode(file_get_contents($this->filePath), true);

        if (is_array($data) === false) {
            throw new \RuntimeException("Json file is invalid");
        }

        return $data;
    }
}

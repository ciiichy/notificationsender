<?php

namespace App\Shared\Infrastructure\Resolver;

interface Resolver
{
    public function resolve(array $payload): array;
}

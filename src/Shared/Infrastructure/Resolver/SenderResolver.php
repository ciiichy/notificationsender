<?php

namespace App\Shared\Infrastructure\Resolver;

use App\Shared\Domain\Sender\Sender;

final class SenderResolver implements Resolver
{
    /** @var array|Sender[] */
    private array $senders;

    public function __construct($senders)
    {
        $this->senders = $senders;
    }

    public function resolve(array $channels): array
    {
        $senders = [];

        foreach ($this->senders as $sender) {
            if (in_array($sender->acceptChannel(), $channels) === true) {
                $senders[] = $sender;
            }
        }
        
        return $senders;
    }
}

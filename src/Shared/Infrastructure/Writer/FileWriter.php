<?php

namespace App\Shared\Infrastructure\Writer;

use App\Shared\Domain\Writer\Writer;

final class FileWriter implements Writer
{
    private string $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function write(string $text): void
    {
        $fp = fopen($this->path, 'a');
        fwrite($fp, $text);
        fclose($fp);
    }
}

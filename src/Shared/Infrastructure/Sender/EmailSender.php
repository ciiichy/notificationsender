<?php

namespace App\Shared\Infrastructure\Sender;

use App\Core\Domain\ContactChannelsTypes;
use App\Shared\Domain\Sender\Sender;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

final class EmailSender implements Sender
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(string $target, string $message): void
    {
        $email = (new Email())
            ->from('send-notification@test.pl')
            ->to($target)
            ->subject("Notification")
            ->text($message);

        $this->mailer->send($email);
        sleep(1);
    }

    public function acceptChannel(): string
    {
        return ContactChannelsTypes::CONTACT_CHANNEL_EMAIL;
    }
}

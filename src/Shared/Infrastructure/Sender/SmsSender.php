<?php

namespace App\Shared\Infrastructure\Sender;

use App\Core\Domain\ContactChannelsTypes;
use App\Shared\Domain\Sender\Sender;
use App\Shared\Domain\Writer\Writer;

final class SmsSender implements Sender
{
    private Writer $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function send(string $target, string $message): void
    {
        $this->writer->write(sprintf('%s: %s'.PHP_EOL, $target, $message));
    }

    public function acceptChannel(): string
    {
        return ContactChannelsTypes::CONTACT_CHANNEL_SMS;
    }
}

<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Infrastructure\Database\DBEngine;

abstract class JsonRepository
{
    protected DBEngine $dbEngine;

    public function __construct(DBEngine $dbEngine)
    {
        $this->dbEngine = $dbEngine;
    }
}

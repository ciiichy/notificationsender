<?php

namespace App\Shared\Application\View;

interface View
{
    public function toArray(): array;
}

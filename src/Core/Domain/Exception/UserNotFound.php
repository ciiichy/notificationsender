<?php

namespace App\Core\Domain\Exception;

use Throwable;

final class UserNotFound extends \InvalidArgumentException
{
    public function __construct(string $login, $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('User with "%s" login not found', $login), $code, $previous);
    }
}

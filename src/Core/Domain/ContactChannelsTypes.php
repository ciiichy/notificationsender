<?php

namespace App\Core\Domain;

final class ContactChannelsTypes
{
    public const CONTACT_CHANNEL_SMS = 'sms';
    public const CONTACT_CHANNEL_EMAIL = 'email';
}

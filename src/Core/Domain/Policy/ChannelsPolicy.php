<?php

namespace App\Core\Domain\Policy;

interface ChannelsPolicy
{
    public function execute(array $channels): array;
}

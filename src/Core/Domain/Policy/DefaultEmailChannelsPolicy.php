<?php

namespace App\Core\Domain\Policy;

use App\Core\Domain\ContactChannelsTypes;

final class DefaultEmailChannelsPolicy implements ChannelsPolicy
{
    private const DEFAULT_CHANNEL = ContactChannelsTypes::CONTACT_CHANNEL_EMAIL;

    public function execute(array $channels): array
    {
        if (empty($channels) === true) {
            return [self::DEFAULT_CHANNEL];
        }

        return $channels;
    }
}

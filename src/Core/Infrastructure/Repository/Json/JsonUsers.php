<?php

namespace App\Core\Infrastructure\Repository\Json;

use App\Core\Application\Repository\Users;
use App\Core\Application\View\UserView;
use App\Shared\Infrastructure\Repository\JsonRepository;

final class JsonUsers extends JsonRepository implements Users
{
    private const CONTACT_CHANNELS_DELIMITER = ",";

    /**
     * @return array|UserView[]
     */
    public function all(): array
    {
        $users = $this->dbEngine->all();

        return array_map(
            function (array $user): UserView
            {
                return new UserView(
                    $user['login'],
                    $user['email'],
                    $user['contact_channels'] !== null ?
                        explode(self::CONTACT_CHANNELS_DELIMITER, $user['contact_channels']) :
                        []
                );
            },
            $users
        );
    }

    public function byLogin(string $login): ?UserView
    {
        $users = $this->all();

        foreach ($users as $user) {
            if ($user->login() === $login) {
                return $user;
            }
        }

        return null;
    }
}

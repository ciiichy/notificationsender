<?php

namespace App\Core\UI\CLI;

use App\Core\Application\Command\SendNotificationCommand;
use App\Core\Application\Query\GetUserByLoginQuery;
use App\Core\Application\View\UserView;
use App\Core\Domain\Exception\UserNotFound;
use App\Shared\UI\CommandBusTrait;
use App\Shared\UI\QueryBusTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Webmozart\Assert\Assert;

class SendNotification extends Command
{
    protected static $defaultName = 'app:send-notification';

    use CommandBusTrait;
    use QueryBusTrait;

    public function __construct(MessageBusInterface $commandBus, MessageBusInterface $queryBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    protected function configure()
    {
        $this
            ->setDescription("Send notification")
            ->addOption("message", "m", InputOption::VALUE_REQUIRED)
            ->addOption("user", "u", InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $login = $input->getOption('user');
        $message = $input->getOption('message');

        Assert::notEmpty($login);
        Assert::notEmpty($message);

        /** @var UserView $user */
        $user = $this->ask(new GetUserByLoginQuery($login));

        if ($user === null) {
            throw new UserNotFound($login);
        }

        $this->exec(
            new SendNotificationCommand(
                $user->email(),
                $message,
                $user->contactChannels()
            )
        );

        return Command::SUCCESS;
    }
}

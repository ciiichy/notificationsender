<?php

namespace App\Core\UI\HTTP\API;

use App\Core\Application\Query\GetUsersQuery;
use App\Core\Application\View\UserView;
use App\Shared\UI\ApiController;
use App\Shared\UI\QueryBusTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;

final class GetAllUsersAction implements ApiController
{
    use QueryBusTrait;

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function __invoke()
    {
        /** @var array $users */
        $users = $this->ask(new GetUsersQuery());

        return new JsonResponse(
            array_map(
                function (UserView $userView): array
                {
                    return $userView->toArray();
                },
                $users
            )
        );
    }
}

<?php

namespace App\Core\UI\HTTP\API;

use App\Core\Application\Command\SendNotificationCommand;
use App\Core\Application\Query\GetUserByLoginQuery;
use App\Core\Domain\Exception\UserNotFound;
use App\Shared\UI\ApiController;
use App\Shared\UI\CommandBusTrait;
use App\Shared\UI\QueryBusTrait;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendNotificationsAction implements ApiController
{
    use CommandBusTrait;
    use QueryBusTrait;

    public function __construct(MessageBusInterface $commandBus, MessageBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function __invoke(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (is_array($data) === false) {
            throw new \RuntimeException("Invalid request");
        }

        $logins = $data['logins'] ?? [];
        $message = $data['message'] ?? null;

        Assert::assertIsString($message);

        if (empty($logins) === true) {
            throw new \RuntimeException("Invalid logins");
        }

        Assert::assertContainsOnly("string", $logins);

        foreach ($logins as $login) {
            $user = $this->ask(new GetUserByLoginQuery($login));

            if ($user === null) {
                throw new UserNotFound($login);
            }

            $this->exec(
                new SendNotificationCommand(
                    $user->email(),
                    $message,
                    $user->contactChannels()
                )
            );
        }

        return new JsonResponse(null);
    }
}

<?php

namespace App\Core\Application\Query;

use App\Shared\Infrastructure\MessageBus\Query;

final class GetUserByLoginQuery implements Query
{
    private string $login;

    public function __construct(string $login)
    {
        $this->login = $login;
    }

    public function login(): string
    {
        return $this->login;
    }
}

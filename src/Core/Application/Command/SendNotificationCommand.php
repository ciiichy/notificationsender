<?php

namespace App\Core\Application\Command;

use App\Shared\Infrastructure\MessageBus\Command;

final class SendNotificationCommand implements Command
{
    private string $target;
    private string $message;
    private array $channels;

    public function __construct(string $target, string $message, array $channels)
    {
        $this->target = $target;
        $this->message = $message;
        $this->channels = $channels;
    }

    public function target(): string
    {
        return $this->target;
    }

    public function channels(): array
    {
        return $this->channels;
    }

    public function message(): string
    {
        return $this->message;
    }
}

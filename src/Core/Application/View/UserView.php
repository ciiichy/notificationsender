<?php

namespace App\Core\Application\View;

use App\Shared\Application\View\View;

class UserView implements View
{
    private string $login;
    private string $email;
    private array $contactChannels;

    public function __construct(string $login, string $email, array $contactChannels)
    {
        $this->login = $login;
        $this->email = $email;
        $this->contactChannels = $contactChannels;
    }

    public function login(): string
    {
        return $this->login;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function contactChannels(): array
    {
        return $this->contactChannels;
    }

    public function toArray(): array
    {
        return [
            'login' => $this->login(),
            'email' => $this->email(),
            'contactChannels' => $this->contactChannels()
        ];
    }
}

<?php

namespace App\Core\Application\Repository;

use App\Core\Application\View\UserView;

interface Users
{
    public function all(): array;

    public function byLogin(string $login): ?UserView;
}

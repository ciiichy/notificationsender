<?php

namespace App\Core\Application\CommandHandler;

use App\Core\Application\Command\SendNotificationCommand;
use App\Core\Domain\Policy\ChannelsPolicy;
use App\Shared\Application\Handler\CommandHandler;
use App\Shared\Domain\Sender\Sender;
use App\Shared\Infrastructure\Resolver\Resolver;

final class SendNotificationHandler implements CommandHandler
{
    private Resolver $senderResolver;
    private ChannelsPolicy $channelsPolicy;

    public function __construct(Resolver $senderResolver, ChannelsPolicy $channelsPolicy)
    {
        $this->senderResolver = $senderResolver;
        $this->channelsPolicy = $channelsPolicy;
    }

    public function __invoke(SendNotificationCommand $command)
    {
        $channels = $this->channelsPolicy->execute($command->channels());

        /** @var Sender $sender */
        foreach ($this->senderResolver->resolve($channels) as $sender){
            $sender->send($command->target(), $command->message());
        }
    }
}

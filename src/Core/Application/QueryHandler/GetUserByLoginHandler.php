<?php

namespace App\Core\Application\QueryHandler;

use App\Core\Application\Query\GetUserByLoginQuery;
use App\Core\Application\Repository\Users;
use App\Core\Application\View\UserView;
use App\Shared\Application\Handler\QueryHandler;

class GetUserByLoginHandler implements QueryHandler
{
    private Users $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function __invoke(GetUserByLoginQuery $query): ?UserView
    {
        return $this->users->byLogin($query->login());
    }
}

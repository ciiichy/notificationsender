<?php

namespace App\Core\Application\QueryHandler;

use App\Core\Application\Query\GetUsersQuery;
use App\Core\Application\Repository\Users;
use App\Shared\Application\Handler\QueryHandler;

class GetUsersHandler implements QueryHandler
{
    private Users $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function __invoke(GetUsersQuery $query): array
    {
        return $this->users->all();
    }
}
